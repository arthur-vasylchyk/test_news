from django.db import models
from django.core.urlresolvers import reverse


# Create your models here.

class News(models.Model):
    title = models.CharField(max_length=100)
    short_text = models.CharField(max_length=1000)
    text = models.CharField(max_length=10000)
    image = models.FileField()

    # for form submit kwargs={'pk': self.pk}
    def get_absolute_url(self):
        return reverse('news:detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.title + ' : ' + self.text
