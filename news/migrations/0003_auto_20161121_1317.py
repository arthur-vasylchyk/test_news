# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-11-21 11:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0002_news_short_text'),
    ]

    operations = [
        migrations.AlterField(
            model_name='news',
            name='image',
            field=models.FileField(upload_to=''),
        ),
        migrations.AlterField(
            model_name='news',
            name='short_text',
            field=models.CharField(max_length=1000),
        ),
    ]
