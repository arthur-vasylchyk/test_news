from django.shortcuts import render, get_object_or_404
from .models import News

from django.views import generic
from .models import News
from django.views.generic.edit import CreateView, UpdateView, DeleteView

# Create your views here.

'''def index(request):
    all_news = News.objects.all();
    return render(request, 'news/index.html', {'all_news': all_news})


def detail(request, news_id):
    news = get_object_or_404(News, pk=news_id)
    return render(request, 'news/detail.html', {'news': news})'''


class IndexView(generic.ListView):
    template_name = 'news/index.html'
    context_object_name = 'all_news'

    def get_queryset(self):
        return News.objects.all()


class DetailView(generic.DetailView):
    model = News
    template_name = 'news/detail.html'


class NewsCreate(CreateView):
    model = News
    fields = ['title', 'short_text', 'text', 'image']
